<?php
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	header("Content-Type: application/json");

	$url = parse_url(getenv("CLEARDB_DATABASE_URL"));

	$server = $url["host"];
	$username = $url["user"];
	$password = $url["pass"];
	$db = substr($url["path"], 1);

	$conn = new mysqli($server, $username, $password, $db);

	if(mysqli_connect_errno()) {
		echo json_encode(array("result" => -1)); // internal error
		exit();
	}

	if($_POST["action"] != 3) {
		$statement = $conn -> prepare("INSERT INTO Records (user_source, user_target, timestamp, action, content) VALUES (?, ?, ?, ?, ?)");
		$statement -> bind_param("ssiis", $_POST["user_source"], $_POST["user_target"], $_POST["timestamp"], $_POST["action"], $_POST["content"]);
		$statement -> execute();
	} else {
		$statement = $conn -> prepare("INSERT INTO Records (user_source, user_target, timestamp, action) VALUES (?, ?, ?, ?)");
		$statement -> bind_param("ssii", $_POST["user_source"], $_POST["user_target"], $_POST["timestamp"], $_POST["action"]);
		$statement -> execute();
	}

	echo json_encode(array("result" => 1)); // success

	$conn -> close();
?>