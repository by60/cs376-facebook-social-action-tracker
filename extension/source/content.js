function parseUserName_fromURL(URL) {
	var baseURL = "https://www.facebook.com/";
	var end_trailing = URL.indexOf("?fref=nf");
	return end_trailing != -1? URL.slice(baseURL.length, end_trailing):URL.slice(baseURL.length) ;
}
function getclick_target(context) {
	var profURL = $(context).closest(".userContentWrapper").find("._5pbw").find("a:last-child").attr("href");
	return parseUserName_fromURL(profURL);
}
function get_comment(context) {
	return $(context).find("._2vxa").find("span").find("span").text();
}
function get_source() {
	var url = $("._2dpe").attr("href");
	return parseUserName_fromURL(url);
}
$(document).ready(function(){
	$("#facebook").on("click", ".share_action_link", function() {
        if(Date.now() > new Date(1435739196000)) return;
    	var source = get_source();
    	var target = getclick_target(this);
    	//console.log(source + " > " + target);
        var formData = {user_source: source, user_target: target, action: 2, content: text, timestamp: Date.now()};
        $.ajax({
            url: "https://bdj.herokuapp.com/save_record.php",
            type: "POST",
            data: $.param(formData)
        });
    });
	$("#facebook").on("click","._1dsp button", function(e) {
        if(Date.now() > new Date(1435739196000)) return;
		var text = $(".uiTextareaAutogrow[name='xhpc_message_text']").val();
    	var source = get_source();
    	var URL = $("._8_2").attr("href"); //gets target URL from cover link 
    	var target = parseUserName_fromURL(URL);
        //console.log(source + " > " + target + ": " + text);
        var formData = {user_source: source, user_target: target, action: 4, content: text, timestamp: Date.now()};
        $.ajax({
            url: "https://bdj.herokuapp.com/save_record.php",
            type: "POST",
            data: $.param(formData)
        });
	});

	$("#facebook").on("keyup",".uiTextareaAutogrow[name='xhpc_message_text']", function(e) {
    	if(Date.now() > new Date(1435739196000)) return;
        var text = $(this).val();
    	var source = get_source();
    	var URL = $("._8_2").attr("href");
    	var target = parseUserName_fromURL(URL);
    	if(e.which != 13) {
	    	localStorage.setItem("wp-"+target,text);
    	}
    	else {
    		//alert("!");
    		text = localStorage.getItem("wp-"+target); 
            //console.log(source + " > " + target + ": " + text);
            var formData = {user_source: source, user_target: target, action: 4, content: text, timestamp: Date.now()};
            $.ajax({
                url: "http://www.aerodroid.com/social_action_tracker/save_record.php",
                type: "POST",
                data: $.param(formData)
            });
    	}
    });
    $("#facebook").on("click", ".UFILikeLink", function() {
    	if(Date.now() > new Date(1435739196000)) return;
        var source = get_source();
    	var target = getclick_target(this);
        var formData = {user_source: source, user_target: target, action: 0, content: "", timestamp: Date.now()};
        $.ajax({
            url: "https://bdj.herokuapp.com/save_record.php",
            type: "POST",
            data: $.param(formData)
        });
    	//console.log(source + " > " + target);
    });
    
    $("#facebook").on("keyup",".UFIAddCommentInput", function(e) {
    	if(Date.now() > new Date(1435739196000)) return;
        var text = get_comment(this);
    	var source = get_source();
    	var target = getclick_target(this);
    	if(e.which != 13) {
    		//console.log(text);
	    	localStorage.setItem("c-"+target,text);
    	}
    	else {
    		//alert("!");
    		text = localStorage.getItem("c-"+target); 
            var formData = {user_source: source, user_target: target, action: 1, content: text, timestamp: Date.now()};
            $.ajax({
                url: "https://bdj.herokuapp.com/save_record.php",
                type: "POST",
                data: $.param(formData)
            });
            //console.log(source + " > " + target + ": " + text);
    	}
    });
    $("#facebook").on("keyup","._552m", function(e) {
    	if(Date.now() > new Date(1435739196000)) return;
        var text = $(this).val();
    	var source = get_source();
    	var URL = $(this).closest(".fbNubFlyoutInner").find("h4.titlebarTextWrapper").find("a").attr("href");
    	var target = parseUserName_fromURL(URL);
    	if(e.which != 13) {
    		
	    	localStorage.setItem("m-"+target,text);
    	}
    	else {
    		text = localStorage.getItem("m-"+target); 
            var formData = {user_source: source, user_target: target, action: 3, content: text, timestamp: Date.now()};
            $.ajax({
                url: "https://bdj.herokuapp.com/save_record.php",
                type: "POST",
                data: $.param(formData)
            });
            //console.log(source + " > " + target + ": " + text);
    	}
    });

});